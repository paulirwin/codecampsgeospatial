﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeCamps.Web.Models
{
    public class State
    {
        public string StateNameShort { get; set; }

        public string StateNameFull { get; set; }
    }
}