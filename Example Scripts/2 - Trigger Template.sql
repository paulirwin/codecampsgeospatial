CREATE TRIGGER <Schema_Name, sysname, dbo>.<Table_Name, sysname, Table_Name>_UpdatePosition
   ON  <Schema_Name, sysname, dbo>.<Table_Name, sysname, Table_Name> 
   AFTER INSERT, UPDATE
AS 
BEGIN
	
	SET NOCOUNT ON;

    UPDATE t
	SET Position = dbo.GeoFromLatLong(wc.Latitude, wc.Longitude)
	FROM <Schema_Name, sysname, dbo>.<Table_Name, sysname, Table_Name> t
	INNER JOIN INSERTED i
		ON i.<Primary_Key_Column, sysname, ID> = t.<Primary_Key_Column, sysname, Primary_Key_Column>
	INNER JOIN WorldCities wc
		ON i.<City_Column, sysname, City> = wc.City
		AND i.<State_Column, sysname, State> = wc.Region
		AND i.<Country_Column, sysname, Country> = wc.Country
END
GO