-- Requires States shapefile from US Census TIGER/Line data created with these options:
-- Table Name: USStates
-- Type: Geography (spheric)
-- Set SRID: 4326
-- Spatial Index: Yes
-- Geometry Name: Position
-- ID column Name: ID

SELECT * FROM USStates;
GO

SELECT state.STUSPS10 AS StateNameShort, state.NAME10 AS StateNameFull
FROM USStates state
WHERE state.Position.STIntersects(dbo.GeoFromLatLong(30.3083758, -81.7217185)) = 1;
GO

-- wrap into re-usable sproc
CREATE PROCEDURE GetStateFromPosition
(
	@Latitude float,
	@Longitude float
)
AS
BEGIN

	SELECT state.STUSPS10 AS StateNameShort, state.NAME10 AS StateNameFull
	FROM USStates state
	WHERE state.Position.STIntersects(dbo.GeoFromLatLong(@Latitude, @Longitude)) = 1;

END
GO

-- Example: get state if you're at the Empire State Building
EXEC GetStateFromPosition 40.748433, -73.985656;
GO
