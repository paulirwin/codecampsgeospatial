-- Requires WorldCities table created from data at http://www.maxmind.com/en/worldcities
-- Use Import/Export Wizard to import flat file data into a table with this schema:
--
-- CREATE TABLE [dbo].[WorldCities](
--	[Country] [nvarchar](2) NULL,
--	[City] [nvarchar](200) NULL,
--	[AccentCity] [nvarchar](200) NULL,
--	[Region] [nvarchar](50) NULL,
--	[Population] [int] NULL,
--	[Latitude] [real] NULL,
--	[Longitude] [real] NULL
-- )
-- 

CREATE TABLE CodeCamps
(
	CodeCampID INT IDENTITY(1, 1) PRIMARY KEY,
	Name NVARCHAR(50) NOT NULL,
	EventDate DATE NOT NULL,
	City NVARCHAR(50) NOT NULL,
	State NVARCHAR(2) NOT NULL,
	Country NVARCHAR(3) NOT NULL
);

INSERT INTO CodeCamps (Name, EventDate, City, State, Country)
VALUES
('Jax Code Camp 2012', '10/6/2012', 'Jacksonville', 'FL', 'US'),
('BarCamp Tampa 2012', '10/13/2012', 'Tampa', 'FL', 'US'),
('RDU Code Camp 2012', '11/10/2012', 'Raleigh', 'NC', 'US'),
('SoCal Code Camp 2012', '10/13/2012', 'Los Angeles', 'CA', 'US'),
('Houston Code Camp 2012', '8/25/2012', 'Houston', 'TX', 'US'),
('Silicon Valley Code Camp 2012', '10/6/2012', 'Los Altos Hills', 'CA', 'US'),
('Code Camp NYC 7', '9/15/2012', 'New York', 'NY', 'US'),
('South Florida Code Camp 2013', '2/9/2013', 'Fort Lauderdale', 'FL', 'US');

SELECT * FROM CodeCamps;

-- Match code camp with lat/long
SELECT *
FROM CodeCamps cc
INNER JOIN WorldCities wc
	ON cc.City = wc.City
	AND cc.State = wc.Region
	AND cc.Country = wc.Country;

-- Add position column
ALTER TABLE CodeCamps
ADD Position geography NULL;
GO

-- Create Helper Functions
CREATE FUNCTION GeoFromLatLong
(
	@Latitude float,
	@Longitude float
)
RETURNS geography
AS
BEGIN
	DECLARE @geo geography = geography::STPointFromText('POINT(' + CAST(@Longitude AS VARCHAR(20)) + ' ' + CAST(@Latitude AS VARCHAR(20)) + ')', 4326);
	RETURN (@geo);
END
GO

CREATE FUNCTION MetersToMiles
(
	@Meters float
)
RETURNS float
AS
BEGIN
	RETURN (SELECT @Meters * 0.000621371192)
END
GO

-- Backfill position
UPDATE cc
SET Position = dbo.GeoFromLatLong(wc.Latitude, wc.Longitude)
FROM CodeCamps cc
INNER JOIN WorldCities wc
	ON cc.City = wc.City
	AND cc.State = wc.Region
	AND cc.Country = wc.Country;

SELECT * FROM CodeCamps;

-- SELECT with distance in miles
SELECT q.Name, q.EventDate, q.City, q.State, q.Country, dbo.MetersToMiles(q.DistanceM) as DistanceMiles
FROM
(
	SELECT cc.*, cc.Position.STDistance(dbo.GeoFromLatLong(city.Latitude, city.Longitude)) as DistanceM
	FROM CodeCamps cc
	INNER JOIN WorldCities city
		ON city.Country = 'US'
		AND city.City = 'Tallahassee'
		AND city.Region = 'FL'
	WHERE cc.EventDate >= GETDATE()
) q
ORDER BY DistanceM
GO

-- Create sproc
CREATE PROCEDURE GetNearbyCodeCamps
(
	@City NVARCHAR(50),
	@State NVARCHAR(2),
	@Country NVARCHAR(3)
)
AS
BEGIN

	SELECT q.CodecampID, q.Name, q.EventDate, q.City, q.State, q.Country, dbo.MetersToMiles(q.DistanceM) as DistanceMiles
	FROM
	(
		SELECT cc.*, cc.Position.STDistance(dbo.GeoFromLatLong(city.Latitude, city.Longitude)) as DistanceM
		FROM CodeCamps cc
		INNER JOIN WorldCities city
			ON city.Country = @Country
			AND city.City = @City
			AND city.Region = @State
		WHERE cc.EventDate >= GETDATE()
	) q
	ORDER BY DistanceM

END
GO

EXEC GetNearbyCodeCamps 'Jacksonville', 'FL', 'US';
GO

-- procedure to get nearby code camps from GPS position
CREATE PROCEDURE GetCodeCampsByPosition
(
	@Latitude float,
	@Longitude float
)
AS
BEGIN

	SELECT q.CodecampID, q.Name, q.EventDate, q.City, q.State, q.Country, dbo.MetersToMiles(q.DistanceM) as DistanceMiles
	FROM
	(
		SELECT cc.*, cc.Position.STDistance(dbo.GeoFromLatLong(@Latitude, @Longitude)) as DistanceM
		FROM CodeCamps cc
		WHERE cc.EventDate >= GETDATE()
	) q
	ORDER BY DistanceM

END
GO

-- nearest code camps to empire state building
EXEC GetCodeCampsByPosition 40.748433, -73.985656;
GO