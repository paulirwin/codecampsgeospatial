﻿using CodeCamps.Metro.Data;
using CodeCamps.Metro.DataModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Split Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234234

namespace CodeCamps.Metro
{
    /// <summary>
    /// A page that displays a group title, a list of items within the group, and details for the
    /// currently selected item.
    /// </summary>
    public sealed partial class SplitPage : CodeCamps.Metro.Common.LayoutAwarePage
    {
        public SplitPage()
        {
            this.InitializeComponent();
            this.DataContext = this;
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var geo = new Geolocator();
            
            Geoposition position = null;

            try
            {
                position = await geo.GetGeopositionAsync();
            }
            catch
            {                
            }

            if (position == null)
            {
                var msg = new MessageDialog("There was an error getting your location.", "Geolocation Error");
                await msg.ShowAsync();
                return;
            }

            var lat = position.Coordinate.Latitude;
            var lon = position.Coordinate.Longitude;

            var url = string.Format("http://127.0.0.2:81/api/codecampsapi?latitude={0}&longitude={1}", lat, lon);

            var wc = new HttpClient();
            string json = null;

            try
            {
                json = await wc.GetStringAsync(url);
            }
            catch
            {
            }

            if (json == null)
            {
                var msg = new MessageDialog("There was an error downloading code camps from the Azure server.", "Web API Error");
                await msg.ShowAsync();
                return;
            }

            var codecamps = await JsonConvert.DeserializeObjectAsync<List<CodeCamp>>(json);

            foreach (var codecamp in codecamps)
            {
                _codeCamps.Add(codecamp);
            }
        }

        private ObservableCollection<CodeCamp> _codeCamps = new ObservableCollection<CodeCamp>();
        public ObservableCollection<CodeCamp> CodeCamps
        {
            get { return _codeCamps; }
            set { _codeCamps = value; }
        }

        private void itemListView_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            
        }
        
    }
}
