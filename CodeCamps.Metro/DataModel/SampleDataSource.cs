﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.Specialized;
using CodeCamps.Metro.DataModel;

namespace CodeCamps.Metro.Data
{
    public sealed class SampleDataSource
    {
        private static SampleDataSource _sampleDataSource = new SampleDataSource();

        private ObservableCollection<CodeCamp> _codeCamps = new ObservableCollection<CodeCamp>();
        public ObservableCollection<CodeCamp> CodeCamps
        {
            get { return this._codeCamps; }
        }
        
        public SampleDataSource()
        {
            _codeCamps.Add(new CodeCamp
            {
                Name = "Jax Code Camp 2012",
                EventDate = "10/6/2012",
                City = "Jacksonville",
                State = "FL",
                Country = "US",
                DistanceMiles = 100,
                Latitude = 30,
                Longitude = -81
            });

            _codeCamps.Add(new CodeCamp
            {
                Name = "South Florida Code Camp 2013",
                EventDate = "2/9/2013",
                City = "Fort Lauderdale",
                State = "FL",
                Country = "US",
                DistanceMiles = 500,
                Latitude = 30,
                Longitude = -81
            });
        }
    }
}
