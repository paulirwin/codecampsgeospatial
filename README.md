#Jax Code Camp 2012
##Geospatial Querying using SQL Server
###Paul Irwin, Lead Software Engineer at feature[23]

In this repo you will find the code used during my talk at Jax Code Camp 2012 on Geospatial Querying using SQL Server and SQL Azure. The CodeCamps.Cloud and CodeCamps.Web projects make up the Azure/HTML5 side, while the CodeCamps.Metro project is the barebones Windows 8/RT client. The SQL scripts used can be found in the Scripts folder.

## Resources
- Free World Cities Database: http://www.maxmind.com/en/worldcities
- SQL Server Spatial Tools: http://www.sharpgis.net/page/SQL-Server-2008-Spatial-Tools.aspx
- TIGER/Line data: http://www.census.gov/geo/www/tiger/tgrshp2010/tgrshp2010.html
